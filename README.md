# Portfolio project IDATA1003 - 2023
This file uses Mark Down syntax. For more information see [here](https://www.markdownguide.org/basic-syntax/).

STUDENT NAME = "Nathalie Graidy Andreassen"  
STUDENT ID = "111751"

## Project description

[//]: #
This project is the finished program for the last project in the subject IDATT1003.
The system is about handling train departures, and has current recirment which are specified in the
report.

## Project structure
The project is organized in the following structure:
src: The source code for the project, including the implementations of the TrainDeparture class and
TrainDepartureRegister class
test: The folder containing JUnit test classes to test the functionality of the classes.
[//]: 


## Link to repository

[//]: # 
https://gitlab.stud.idi.ntnu.no/nathalga/idatt1003-2023-mappevurdering

## How to run the project

[//]: # 
The main class is the TrainDispatchApp class. To run the system the user has to have Java
and an IDE installed to their computer. After that the user runs the TrainDispatchapp file.

## How to run the tests

[//]: # 
To run the tests the user has to have Java and an IDE installed to their computer. After
that the user runs the test files.

## References

[//]: # 
My references are my fellow students, and I have used ChatGPT to right the tests for the
verier class.

